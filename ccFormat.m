function [ nzval, rowind, colptr ] = ccFormat (A)

	[n n]=size(A);
	nzval=zeros(0,0);
	rowind=zeros(0,0);
	colptr(1)=1;

	for i=1:n
		[x y z]=find(A(:,i)');

		nzval=[ nzval z ];
		rowind=[ rowind y ];

		colptr(i+1)=colptr(i)+length(y);
	endfor
endfunction