%matrice inferior triunghiulara
function x = triCL (A,b)
 
 		A = tril (A);
        [nzval,colind,rowptr]=clFormat(A);
        [n n] = size (A);
        x = b;
        for i=1:length(rowptr)-1
                for j=rowptr(i):rowptr(i+1)-2;
                        x(i)=x(i)-nzval(j)*x(colind(j));
                end
                x(i)=x(i)/nzval(rowptr(i+1)-1);
 
        end
endfunction
