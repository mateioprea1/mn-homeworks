function x = triCC (A,b)
A=tril (A)
	[ nzval,rowind,colptr ] = ccFormat (A);
	[n n]=size (A);
	x=b
	for i=1:n
	x(i)=x(i)/nzval(colptr(i));
		for j=colptr(i)+1:colptr(i+1)-1
		x(rowind(j))=x(rowind(j))-nzval(j)*x(i);
		end
	end
endfunction
