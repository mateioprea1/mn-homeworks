function cod=corectareHam(sir)
pow=[];
for i=1:length(sir)
	pow(i)=0;
	if sir(i)=='1'
		var1=i;
		p2=1;
		while(var1>0)
			if(mod(var1,2)==1)
				pow(p2)++;
			end
			p2=p2*2;	
			var1=floor(var1/2);
		endwhile
	endif
endfor

p2=1;
sol=0;
cod=sir;
for i = 1:length(sir)
	if(i==p2)
		p2=p2*2;
		if mod(pow(i),2)==1 && sir(i)== '0'
			sol+=i;
		elseif mod(pow(i),2)==0 && sir(i)== '1'
			sol+=i;
		endif
	endif
endfor
if(cod(sol)=='1')
	cod(sol)='0';
else
	cod(sol)='1';
endif


end
