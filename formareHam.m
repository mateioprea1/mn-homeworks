function cod = formareHam(sir)
cod="p";
poz=2;
index=1;
pow=[1];
for i=1:length(sir)
	
	if(index==poz)
	
		index++;
		cod(index)='p';
		poz=poz*2;
		pow(index)=0;
	end
	index++;
	cod(index)=sir(i);
	pow(index)=0;
	if sir(i)=='1'
		var1=index;
		p2=1;
		while(var1>0)
			if(mod(var1,2)==1)
				pow(p2)++;
			end
			p2=p2*2;	
			var1=floor(var1/2);
		endwhile
	endif
endfor
for i=1:length(cod)
	if(cod(i)=='p')
		if mod(pow(i),2)==0
			cod(i)='0';
		else
			cod(i)='1';
		end
end	
end
