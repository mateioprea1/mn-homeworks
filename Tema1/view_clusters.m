% outputs a graphical representation of the clustering solution
function view_clusters(points, centroids)
	dim=size(points,1);
	culori=zeros(dim,3);
	NC=size(centroids)(1);
	defaulculori=rand(NC,3);
		for j=1:dim
			for i=1:NC
		d(i)=norm(centroids(i,:)-points(j,:),2);
			endfor
		[m n]=min (d);

			vector(j)=n;
			culori(j,:)=defaulculori(n,:);
		endfor
			
	scatter3(points(:, 1), points(:, 2), points(:, 3), [], culori);
	% TODO graphical representation coded here
end
