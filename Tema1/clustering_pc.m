% computes the NC centroids corresponding to the given points using K-Means
function centroids = clustering_pc(points, NC)
	centroids = [];

%Valoarea initiala a centroizilor
for i=1:NC 
	a(i,:)=points(i,:);
end
	centroids=zeros(NC,3);
	%vector_a=zeros(NC,1);
	while centroids~=a
		centroids=a;
		%calculam distanta 
		dim=size(points,1);
		for j=1:dim
			for i=1:NC
		d(i)=norm(a(i,:)-points(j,:),2);
			endfor
		[m n]=min (d);

			vector(j)=n;
		endfor
			
			for z=1:NC
			
			idpoints=find(vector == z );
			if length(idpoints)==1
			a(z,:)=points(idpoints,:);
			else
			nr=size(idpoints)(2);
			if(nr>0)
			a(z,:)=sum(points(idpoints,:)./nr);
			endif
			endif
			endfor
			
	endwhile

centroids=a;


endfunction
