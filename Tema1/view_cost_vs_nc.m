function view_cost_vs_nc(file_points)
	load (file_points);
	x=1:10;
	y=1:10;
	for i=1:10
		centroids=clustering_pc(points,i);
		y(i)=compute_cost_pc(points,centroids);
	endfor
	plot(x,y);
end

