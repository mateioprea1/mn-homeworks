function [nzval, colind, rowptr ] = clFormat (A)

	[n n]=size(A);
	rowptr(1)=1;
	nzval = zeros (0,0); %initializam nzval [](0x0)
	colind = zeros (0,0); %initializam colind [](0x0)

		for i=1:n
			[x y z]=find(A(i,:)); % returnam locatia indicilor nenuli

			nzval = [nzval z]; % comenteaza ce face asta
			colind = [colind y]; % comeneteaza ce face asta
			rowptr(i+1)=rowptr(i) + length(y); %  comenteaza ce face asta
		endfor
endfunction
