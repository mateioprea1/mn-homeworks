function nearest_neighbor (image)

A=imread (image);

[m n]=size(A);
x=zeros(2*m,2*n);
for i=1:m
	for j=1:n
		x(2*(i-1)+1:2*i,2*(j-1)+1:2*j)=A(i,j)*ones(2,2);
	end;
end;
imwrite (uint8(x),strcat("out_nn_",image(10:length(image))));

endfunction
