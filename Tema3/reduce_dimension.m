function reduce_dimension (image,rate)

a=imread(image);
x=a.*(1-rate/100);
imwrite (x,strcat('out_rd_',num2str(rate),'_',image));

endfunction
