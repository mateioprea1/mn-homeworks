function crop (image)

A=imread (image);
[m n]=size(A);
cropped_image=A((m/4)+1:(3*m/4),n/4+1:3/4*n);
imwrite (cropped_image,strcat('out_crop_',image));

endfunction
