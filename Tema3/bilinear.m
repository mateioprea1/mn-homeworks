function bilinear (image)

A=imread (image);
A=single(A);
[m n]=size(A);
x=(zeros(2*m,2*n));
x=single(x);
for i=1:m
	for j=1:n
		x(2*(i-1)+1:2*i,2*(j-1)+1:2*j)=A(i,j);
	end;
end;
y=zeros(2*m-1,2*n-1);
y=single(y);
y=uint8((x(1:2*m-1,1:2*n-1)+x(1:2*m-1,2:2*n)+x(2:2*m,1:2*n-1)+x(2:2*m,2:2*n))/4);
imwrite (y,strcat("out_billinear_",image(10:length(image))));
endfunction