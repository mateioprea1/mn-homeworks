function add_zero ( image ) 

A=imread (image);
[m n]=size(A);
x=uint8(zeros(2*m-1,2*n-1));
x(1:2:2*m-1,1:2:2*n-1)=A;
imwrite (x,strcat("out_add_zero_",image(10:length(image))));
endfunction
